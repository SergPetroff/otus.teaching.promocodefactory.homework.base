﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T> : IRepository<T>
        where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<IEnumerable<T>> DeleteByIdAsync(Guid id)
        {
            var modified = Data.Where(u => u.Id != id).ToList();
            this.Data = modified;
            return Task.FromResult(Data);
        }


        public Task<T> UpdateEmployee(Employee newdata)
        {
            var findempl = Data.FirstOrDefault(x => x.Id == newdata.Id);
            
            return Task.FromResult(findempl);
        }


        public Task<T> AddEmployee(Employee newemployee)
        {
            var alldata = Data;

            List<T> newlist = Data.ToList();
            newlist.Add(newemployee as T);
            Data = newlist;
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == newemployee.Id));
        }




    }
}
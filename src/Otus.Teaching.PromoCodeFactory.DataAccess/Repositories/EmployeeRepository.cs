﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EmployeeRepository : IRepositoryEmployee
    {
        private readonly AppDBContext _ctx;

        public EmployeeRepository(AppDBContext ctx)
        {
            _ctx = ctx;
        }
        public async Task<Employee> AddEmployee(Employee newemployee)
        {
            _ctx.Employees.Add(newemployee);
            await _ctx.SaveChangesAsync();
            return newemployee;
        }

        public Task<Employee> DeleteByIdAsync(Guid id)
        {
            throw new NotImplementedException();
        }

        public async Task<List<Employee>> GetAllAsync()
        {
            return await _ctx.Employees.ToListAsync();
        }

        public async Task<Employee> GetByIdAsync(Guid id)
        {
            return await _ctx.Employees.FirstOrDefaultAsync(e => e.Id == id);
        }

        public Task<Employee> UpdateEmployee(Employee updatedata)
        {
            throw new NotImplementedException();
        }
    }
}

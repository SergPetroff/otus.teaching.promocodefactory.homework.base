﻿using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IRepositoryEmployee
    {
        Task<List<Employee>> GetAllAsync();

        Task<Employee> GetByIdAsync(Guid id);


        Task<Employee> DeleteByIdAsync(Guid id);

        Task<Employee> UpdateEmployee(Employee updatedata);


        Task<Employee> AddEmployee(Employee newemployee);
    }

    
}
